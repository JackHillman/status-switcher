import { Transport } from './transport'
import { Integration, Status } from 'status-switcher-core'

type Profile = {
  status_text: string
  status_emoji: string
  status_expiration?: number
}

export class Slack implements Integration {
  private transport: Transport

  constructor() {
    this.transport = new Transport()
  }

  async setStatus(status: Status): Promise<void> {
    await this.transport.client.users.profile.set({
      profile: JSON.stringify(this.buildProfile(status))
    })
  }

  buildProfile(status: Status): Profile {
    switch (status) {
      case Status.ACTIVE:
        return { status_text: '', status_emoji: '' }
      case Status.AWAY:
        return { status_text: 'Away from keyboard', status_emoji: ':brb:' }
      case Status.LUNCH:
        return { status_text: 'On lunch', status_emoji: ':sandwich:' }
      case Status.OFFLINE:
        return { status_text: 'Offline', status_emoji: ':zzz:' }
      case Status.MEETING:
        return { status_text: 'In a meeting', status_emoji: ':virtual-meeting:' }
      case Status.DEEP_WORK:
        return { status_text: 'Deep work zone', status_emoji: ':heads-down:' }
    }
  }
}
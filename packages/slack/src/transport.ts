import { WebClient } from '@slack/web-api'

export class Transport {
  public readonly client: WebClient

  constructor() {
    this.client = new WebClient(process.env.SLACK_TOKEN)
  }
}
import { Status } from '../status'

export interface Integration {
  setStatus(status: Status): Promise<void>
}
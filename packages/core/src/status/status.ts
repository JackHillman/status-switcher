export enum Status {
  ACTIVE = 'active',
  AWAY = 'away',
  LUNCH = 'lunch',
  OFFLINE = 'offline',
  MEETING = 'meeting',
  DEEP_WORK = 'deep-work',
}

export const statusList = ['active', 'away', 'lunch', 'offline', 'meeting', 'deep-work']

export const isStatus = (x: string): x is Status => {
  return statusList.includes(x)
}

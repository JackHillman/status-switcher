import { Integration } from 'status-switcher-core'
import { Slack } from 'status-switcher-slack'

export const integrations: Integration[] = [
  new Slack(),
]
#!/usr/bin/env node

import { Command } from 'commander'
// @ts-ignore
import { notify } from 'node-notifier'
import { isStatus, statusList } from 'status-switcher-core'
import { integrations } from './integrations'
import { icon } from './icons'

const program = new Command('status switcher')

program.command('set <status>')
  .action(async (status) => {
    if (!isStatus(status)) {
      console.error(`Invalid status '${status}', please select one of:`)
      statusList.map(s => console.error(`  ${s}`))
      process.exit(1)
    }

    console.log(`Changing to status ${status}`)
    await Promise.all(integrations.map(i => i.setStatus(status)))
    console.log('Done!')

    notify({
      title: 'Status changed',
      message: `Changed status to '${status}'`,
      icon: icon(status),
    })
  })

program.parse(process.argv)
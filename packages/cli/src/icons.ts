import path from 'path'
import { Status } from 'status-switcher-core'

export type FilePath = string

const asset = (filename: string): FilePath => path.resolve(__dirname, '../assets/', filename)

export const statusIcon: Record<Status, FilePath> = {
  active: undefined,
  away: asset('brb.png'),
  'deep-work': asset('dwz.png'),
  lunch: asset('lunch.png'),
  meeting: asset('meeting.png'),
  offline: asset('away.png'),
}

export const icon = (status: Status): FilePath => statusIcon[status]